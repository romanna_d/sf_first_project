trigger SetPrimaryTrigger on AccountContact__c (before insert, after insert, after update) {
    if (trigger.isInsert && trigger.isBefore){
        AccountContact__c newRecord = trigger.new[0];        
        List<AccountContact__c> recordList = new List<AccountContact__c>();
        recordList = [SELECT Contact__c FROM AccountContact__c WHERE Contact__c = :newRecord.Contact__c];
        if (recordList.size() == 0) {           
            newRecord.IsPrimary__c = true;}
    }
    if (trigger.isInsert && trigger.isAfter){
        AccountContact__c newRecord = trigger.new[0];
        if (newRecord.IsPrimary__c == true){
            List<AccountContact__c> recordList = [SELECT Name, Contact__c, IsPrimary__c FROM AccountContact__c WHERE Contact__c = :newRecord.Contact__c AND IsPrimary__c = true AND Name != :newRecord.Name];
            if (recordList.size() > 0){
                for(AccountContact__c rec : recordList){                        
                    rec.IsPrimary__c = false;
                }
                update recordList;
            }
        }
    }

    if(trigger.isUpdate){
        AccountContact__c oldRecord = trigger.old[0];
        AccountContact__c newRecord = trigger.new[0];                
            if (oldRecord.IsPrimary__c == false && newRecord.IsPrimary__c == true){                
                List<AccountContact__c> recordList = [SELECT Name, Contact__c, IsPrimary__c FROM AccountContact__c WHERE Contact__c = :newRecord.Contact__c AND IsPrimary__c = true AND Name != :newRecord.Name];                
                if (recordList.size() > 0){
                    for(AccountContact__c rec : recordList){                        
                        rec.IsPrimary__c = false;
                    }                    
                    update recordList;
                }
            }      
            if(oldRecord.IsPrimary__c == true && newRecord.IsPrimary__c == false){                
                List<AccountContact__c> recordList = [SELECT Name, Contact__c, IsPrimary__c FROM AccountContact__c WHERE Contact__c = :newRecord.Contact__c AND IsPrimary__c = true];                
                if (recordList.size() == 0){
                    AccountContact__c lastWorkPlace = [SELECT Name, IsPrimary__c FROM AccountContact__c WHERE Contact__c = :newRecord.Contact__c ORDER BY CreatedDate DESC] [0];                    
                    lastWorkPlace.IsPrimary__c = true;                    
                    update lastWorkPlace;
                }
            }
        }    
}