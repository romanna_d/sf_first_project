trigger LogTrigger on LogEvent__e (after insert) {
    new HandlerExceptions(Trigger.new).handle();
}