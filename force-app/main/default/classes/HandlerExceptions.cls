public with sharing class HandlerExceptions{
    private List<LogEvent__e> events;

    public HandlerExceptions(List<LogEvent__e> events){
        this.events = events;
    }


    public void handle(){
        List<Log__c> records = new List<Log__c>();
        for(LogEvent__e e:events){
            Log__c log = new Log__c();
            log.Logging_level__c = e.Logging_level__c;
            log.Message__c = e.Message__c;
            if (e.Logging_level__c == 'ERROR'){
                log.Context_Id__c = e.Context_Id__c;
                log.Context_Name__c = e.Context_Name__c;
                log.Stack_trace_String__c = e.Stack_trace_String__c;
                log.User_Name__c = e.User_Name__c;
            }
            records.add(log);          
        }
        insert records;
    }
}