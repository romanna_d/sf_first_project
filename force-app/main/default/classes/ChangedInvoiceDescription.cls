public class ChangedInvoiceDescription {
    public static void updateInvoice() {      
      Invoice[] oldInvoice = [SELECT Id, Description FROM Invoice ORDER BY CreatedDate ASC LIMIT 2];      
      for (Invoice inv : oldInvoice) {
          inv.Description = 'new Description';      }
      // save the change you made
      update oldInvoice;
    }

}