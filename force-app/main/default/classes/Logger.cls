public with sharing class Logger{
   
   public static void log(Database.BatchableContext context, Exception ex, LoggingLevel level){
    LogEvent__e logEvent = new LogEvent__e();
    logEvent.Logging_level__c = level.name();
    logEvent.Message__c = ex.getMessage();
    logEvent.Stack_trace_String__c = ex.getStackTraceString();       
    AsyncApexJob job = [SELECT Id, ApexClassId FROM AsyncApexJob WHERE Id = :context.getJobId()];
    ApexClass aC = [SELECT Id, Name, CreatedBy.Name FROM ApexClass WHERE Id =: job.ApexClassId];
    //User u = [SELECT Id, Name FROM User WHERE Id = : job.CreatedBy];
    logEvent.Context_Name__c = aC.Name;
    logEvent.Context_Id__c = context.getJobId();
    logEvent.User_Name__c = aC.CreatedBy.Name;   
    EventBus.publish(logEvent);
}

   public static void Log(String message, LoggingLevel level){
        LogEvent__e logEvent = new LogEvent__e();
        logEvent.Logging_level__c = level.name();
        logEvent.Message__c = message;
        EventBus.publish(logEvent);
   }
}